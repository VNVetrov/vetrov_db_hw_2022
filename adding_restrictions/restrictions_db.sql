-- not null restrictions
ALTER TABLE deliveryman ALTER COLUMN transport_id SET NOT NULL;
ALTER TABLE deliveryman ALTER COLUMN category_license SET NOT NULL;
ALTER TABLE transport ALTER COLUMN plate_number SET NOT NULL;
ALTER TABLE "order" ALTER COLUMN deliveryman_id SET NOT NULL;
ALTER TABLE "order" ALTER COLUMN client_id SET NOT NULL;
ALTER TABLE "order" ALTER COLUMN location_id SET NOT NULL;
ALTER TABLE location ALTER COLUMN country SET NOT NULL;
ALTER TABLE location ALTER COLUMN city SET NOT NULL;
ALTER TABLE location ALTER COLUMN street SET NOT NULL;
ALTER TABLE location ALTER COLUMN house_number SET NOT NULL;

-- checks
ALTER TABLE deliveryman ADD CONSTRAINT dm__c_rating CHECK (rating > 0);
ALTER TABLE client ADD CONSTRAINT c__c_rating CHECK (rating > 0);

-- primary key setting
ALTER TABLE "order" ADD CONSTRAINT o__pk_id PRIMARY KEY (id);