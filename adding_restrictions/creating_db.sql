CREATE DATABASE delivery_service;

CREATE TABLE deliveryman(
    id serial primary key,
    first_name varchar(20),
    last_name varchar(20),
    age int,
    transport_id int,
    category_license varchar(20),
    experience_years int,
    phone_number varchar(11),
    rating float
);

CREATE TABLE transport(
    id serial primary key,
    type varchar(20),
    mark varchar(20),
    color varchar(20),
    plate_number varchar(20),
    mileage varchar(25)
);

CREATE TABLE "order"(
	id serial,
    deliveryman_id int,
    client_id int,
    price decimal,
    location_id int,
    departure_time time,
    arrival_time time
);

CREATE TABLE client(
    first_name varchar(20),
    last_name varchar(20),
    phone_number varchar(20),
    card_number varchar(20),
    rating float
);

CREATE TABLE location(
    id serial primary key,
    country varchar(30),
    city varchar(30),
    street varchar(30),
    house_number varchar(10)
);