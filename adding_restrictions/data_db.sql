INSERT INTO deliveryman (first_name, last_name, transport_id,
                    category_license, experience_years,
                    phone_number, rating, age) VALUES
            ('Vasya', 'Pupkin', 1, 'economy', 1, '89871112231', 4.5, 34),
            ('Petya', 'Lopuk', 2, 'business', 4, '89871112232', 4.8, 39),
            ('Ivan', 'Ivanov', 3, 'economy', 2, '89871112233', 3.4, 27),
            ('Nikita', 'Smirnov', 4, 'business', 3, '89871112234', 3.8, 52),
            ('Andrew', 'Kuznetsov', 5, 'economy', 1, '89871112235', 1.6, 56);

INSERT INTO transport (type, mark, color, plate_number, mileage) VALUES
            ('car','Kia Rio', 'white', 'C528XC', '150000'),
            ('car','Toyota Land Cruser', 'black', 'K558XC', '20000'),
            ('car','Kia Rio', 'black', 'A006KA', '90000'),
            ('car','Hyundai Solaris', 'black', 'H787OY', '500000'),
            ('car','Hyundai Solaris', 'white', 'O315MT', '235555');

INSERT INTO "order" (deliveryman_id, client_id, price, location_id,
                     departure_time, arrival_time) VALUES
            (1, 1, 267, 1, '23:34', '00:00'),
            (2, 2, 690, 2, '18:34', '19:10'),
            (3, 3, 195, 3, '15:23', '15:37'),
            (4, 4, 430, 4, '02:47', '03:02'),
            (5, 5, 234, 5, '08:25', '08:45');

INSERT INTO client (first_name, last_name, phone_number,
                    card_number, rating) VALUES
            ('Nikita', 'Ivanov','80008881101', '4719 5600 1314 9031', 4.7),
            ('Ivan', 'Smirnov','80008881101', '4719 5600 1314 9031', 3.5),
            ('Nikita', 'Fominikh','80008881101', '4719 5600 1314 9031', 4.3),
            ('Petya', 'Egorov','80008881101', '4719 5600 1314 9031', 4.9),
            ('Egor', 'Ivanov','80008881101', '4719 5600 1314 9031', 4.2);

INSERT INTO location (country, city, street,
                              house_number) VALUES
            ('Russia', 'Kazan', 'Parina', 22),
            ('Russia', 'Kazan', 'Bauman', 70),
            ('Russia', 'Kazan', 'Kremlin', 35),
            ('Russia', 'Kazan', 'Kutuya', 46),
            ('Russia', 'Kazan', 'Profsoyuznaya', 42);
