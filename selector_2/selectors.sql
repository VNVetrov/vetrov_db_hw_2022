--1. Вывести список самолетов в порядке убывания количества мест

SELECT aircraft_code, count(seat_no) AS S
FROM aircrafts_data NATURAL JOIN seats
GROUP BY aircraft_code
ORDER BY S DESC;
 
--2. Вывести список самолетов в порядке возрастания соотношения количества мест бизнес-класса к количеству мест эконом-класса

SELECT aircraft_code,
       numeric_div(SELECT count(fare_conditions) FROM aircrafts_data WHERE fare_conditions = 'Business',
                  SELECT count(fare_conditions) FROM aircrafts_data WHERE fare_conditions = 'Economy' ) as S
FROM aircrafts_data NATURAL JOIN seats
GROUP BY aircraft_code
ORDER BY s ASC;
 
--3. Вывести список самолетов в порядке убывания количества полетов

SELECT af.aircraft_code, count(flight_id) AS S
FROM aircrafts_data af
JOIN flights f
    ON af.aircraft_code = f.aircraft_code
GROUP BY af.aircraft_code
ORDER BY S DESC;
 
--4. Вывести список самолетов со средним временем продолжительности полета каждого
SELECT aircraft_code, AVG(AGE(actual_arrival, actual_departure))
FROM flights
GROUP BY aircraft_code;
 
--5. Вывести среднее время задержки вылета в каждом аэропорту

SELECT airport_code, AVG(AGE(actual_departure, scheduled_departure))
FROM flights f
JOIN airports_data ad
    ON f.departure_airport = ad.airport_code
GROUP BY airport_code;

--6. Вывести наименьшее и наибольшее суммарное время задержки вылета в каждом аэропорту

SELECT airport_code,
       MAX(AGE(actual_departure, scheduled_departure)),
       MIN(AGE(actual_departure, scheduled_departure)),
       SUM(AGE(actual_departure, scheduled_departure))
FROM flights f
JOIN airports_data ad
    ON f.departure_airport = ad.airport_code
GROUP BY airport_code;
 
--7. Вывести количество бронирований вылета из Москвы (доступ к значению города из JSON объекта airport_name -> 'ru' )

SELECT count(DISTINCT b.book_ref) as Moscow
FROM airports_data ad
		 JOIN flights f on ad.airport_code = f.departure_airport
         JOIN ticket_flights tf on f.flight_id = tf.flight_id
         JOIN tickets t on tf.ticket_no = t.ticket_no
         JOIN bookings b on t.book_ref = b.book_ref
where city::json ->> 'ru' = 'Москва';
 
--8. Вывести все полеты между городами в разных временных поясах за один день (любой)

SELECT flight_no
FROM flights
         JOIN airports_data arr ON flights.arrival_airport = arr.airport_code
         JOIN airports_data dep ON flights.departure_airport = dep.airport_code
where dep.timezone != arr.timezone AND flights.scheduled_arrival::date = '2017-09-10';
 
 
--9. Вывести данные пассажиров и даты перелетов в/из Москвы у которых в номере паспорта есть сочетание 473
 
SELECT passenger_name, passenger_id, actual_arrival
FROM tickets t
         JOIN ticket_flights tf ON t.ticket_no = tf.ticket_no
         JOIN flights f ON tf.flight_id = f.flight_id
         JOIN airports_data ad ON f.arrival_airport = ad.airport_code
where city::json ->> 'ru' = 'Москва'
  AND split_part(passenger_id, ' ', 2) LIKE '%473%';
 
-----10. Вывести всю информацию по конкретному бронированию (код бронирования, дата, сумма, номера билетов,
-- имя, паспорт пассажиров, время вылета, аэропорт вылета, время прилета, аэропорт прилета, модель самолета).
-- Код бронирования выбрать любой.

SELECT b.book_ref,
       book_date,
       total_amount,
       t.ticket_no,
       passenger_name,
       passenger_id,
       departure_airport,
       actual_departure,
       arrival_airport,
       model
FROM bookings b
         JOIN tickets t ON b.book_ref = t.book_ref
         JOIN ticket_flights tf ON t.ticket_no = tf.ticket_no
         JOIN flights f ON tf.flight_id = f.flight_id
         JOIN airports_data ad ON f.departure_airport = ad.airport_code
         JOIN aircrafts_data a ON f.aircraft_code = a.aircraft_code
WHERE b.book_ref = '...'
 