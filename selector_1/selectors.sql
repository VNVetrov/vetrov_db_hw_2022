--1
SELECT AVG(amount), MIN(amount), MAX(amount) FROM ticket_flights tf
JOIN flights f ON f.flight_id = tf.flight_id
WHERE scheduled_departure::date BETWEEN '2017-08-06' AND '2017-08-10'

--2
SELECT AVG(ticket_sum), MIN(ticket_sum), MAX(ticket_sum) FROM
	(SELECT ticket_no, SUM(amount) AS ticket_sum FROM ticket_flights tf
	JOIN flights f ON f.flight_id = tf.flight_id
	WHERE scheduled_departure::date BETWEEN '2017-08-06' AND '2017-08-10'
	GROUP BY ticket_no) tbl
	
--3
SELECT fare_conditions, AVG(amount), MIN(amount), MAX(amount) FROM ticket_flights tf 
JOIN flights f ON tf.flight_id = f.flight_id
WHERE flight_no IN ('PG0521', 'PG0118' , 'PG0210')
GROUP BY fare_conditions

--4
SELECT * FROM
	(SELECT departure_airport, COUNT(*) FROM flights 
	GROUP BY departure_airport) tbl
JOIN airports ON departure_airport = airport_code