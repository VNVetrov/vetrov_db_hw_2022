# 1) Построить объектную модель предметной области по сформированной ранее
#   концептуальной модели
# 2) Наполнить данными и сериализовать их в JSON файл
# 3) Десериализовать JSON файл в объекты
# 4) Наполнить данными и сериализовать их в XML файл
# 5) Десериализовать XML файл в объекты

import json
from pprint import pprint

import pyconvert.pyconv
import xmltodict


# 1) Построить объектную модель предметной области по сформированной ранее
#  концептуальной модели
class Deliveryman:
    def __init__(self, first_name, last_name
                 , category_license,
                 experience_years, phone_number,
                 rating, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.category_license = category_license
        self.experience_years = experience_years
        self.phone_number = phone_number
        self.rating = rating


class Transport:
    def __init__(self, deliveryman, mark, color, plate_number,
                 mileage):
        self.deliveryman = deliveryman
        self.mark = mark
        self.color = color
        self.plate_number = plate_number
        self.mileage = mileage


class Order:
    def __init__(self, deliveryman, client, price,
                 location, departure_time, arrival_time):
        self.deliveryman = deliveryman
        self.location = location
        self.client = client
        self.price = price
        self.departure_time = departure_time
        self.arrival_time = arrival_time


class Client:
    def __init__(self, first_name, last_name, phone_number,
                 card_number, rating):
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.card_number = card_number
        self.rating = rating


class Location:
    def __init__(self, country, city, street, house_number):
        self.country = country
        self.city = city
        self.street = street
        self.house_number = house_number


class DataBase:
    def __init__(self, orders):
        self.orders = orders


# 2) Наполнить данными и сериализовать их в JSON файл

driver1 = Deliveryman("Vasya", "Pupkin", "econom", 5, "+89178923333", 4, 56)
car1 = Transport(driver1, "Lada Granta", "white", "С666TX", 90000)
client1 = Client("Petya", "Peshekhodov", "+89178923399", "4276 4334 3454 7325", 5)
location1 = Location("Russia", "Kazan", "Parina", 22)
order1 = Order(driver1, client1, 700, location1, "13:55", "15:10")

driver2 = Deliveryman("Sasha", "Morozov", "business", 3, "+89178923336", 5, 34)
car2 = Transport(driver2, "Kia Rio", "black", "X777EH", 54000)
client2 = Client("Ivan", "Medvedev", "+89178923399", "4276 4341 3474 7825", 5)
location2 = Location("Russia", "Kazan", "Kremlevskaya", 35)
order2 = Order(driver2, client2, 900, location2, "14:23", "15:20")

data_base = DataBase([order1, order2])

objects = [driver1, driver2, car1, car2, client2, client1, location1, location2, order2, order1]

r = json.dumps(data_base, default=lambda x: x.__dict__, ensure_ascii=False, indent=2)

file = open("data.json", "w")
file.write(r)
file.close()

# 3) Десериализовать JSON файл в объекты
d = json.loads(r)

# 4) Наполнить данными и сериализовать их в XML файл
xml = str(pyconvert.pyconv.convert2XML(data_base).toprettyxml())

f = open("data.xml", "w")
f.write(xml)
f.close()

# 5) Десериализовать XML файл в объекты
dicts = xmltodict.parse(xml)

# 6) Фильтр
while True:
    is_found = False
    x = input("Введите значение атрибута\n")
    x = int(x) if x.isdigit() else x
    for obj in objects:
        if x in obj.__dict__.values():
            is_found = True
            print(type(obj).__name__)
            pprint(obj.__dict__, indent=2, depth=2)
    if not is_found:
        print("Экземпляра с таким атрибутом не нашлось!")
