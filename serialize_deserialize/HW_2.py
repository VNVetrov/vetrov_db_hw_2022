import json
import os.path

import xmltodict as xmltodict

TAB = '\t'


def dict_to_objs(dictionary: dict, name):
    attr_dict = dict()
    for k, v in dictionary.items():
        if isinstance(v, dict):
            obj = dict_to_objs(v, k)
            attr_dict[k] = obj
        elif isinstance(v, list):
            for idx, item in enumerate(v):
                obj = dict_to_objs(item, f"{k}_{idx}")
                attr_dict[f"{k}_{idx}"] = obj
        else:
            attr_dict[k] = v
    obj = type(str(name), (), attr_dict)
    return obj


def print_obj(obj, depth=0):
    for k, v in obj.__dict__.items():
        if isinstance(v, (int, float, str)):
            if '__' not in k:
                print(f"{TAB * depth}{k}: {v}")
        elif not str(k).startswith('__'):
            depth += 1
            print(f"{TAB * (depth - 1)}{k}: \n", end="")
            print_obj(v, depth)
            depth -= 1


def main():
    file_name = input("Введите имя файла:")
    ext = os.path.splitext(file_name)[1]
    if ext.lower() == '.json':
        with open(file_name, 'r') as f:
            dictionary = json.load(f)
    elif ext.lower() == '.xml':
        with open(file_name, 'rb') as f:
            dictionary = xmltodict.parse(f)
    else:
        print("Неверный ввод, попробуйте еще раз!")
        exit()
    obj = dict_to_objs(dictionary, file_name)
    print_obj(obj)


if __name__ == "__main__":
    main()
