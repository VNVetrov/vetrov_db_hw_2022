CREATE OR REPLACE FUNCTION airports_data_t_bi()
RETURNS TRIGGER
LANGUAGE 'plpgsql' AS
$BODY$
    BEGIN
        new.airport_code = upper(new.airport_code);
        return new;
    END;
$BODY$;

CREATE TRIGGER t_bi_airports_data
    before insert
    on bookings.airports_data
    for each row
    execute function bookings.airports_data_t_bi();