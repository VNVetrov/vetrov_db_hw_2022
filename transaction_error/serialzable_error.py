import threading

import psycopg2

DB_SETTINGS = {
    "dbname": 'demo',
    "user": 'postgres',
    "password": '1',
    "host": 'localhost',
    "port": 5432
}


class Database:
    def __init__(self, name):
        self._con = psycopg2.connect(**DB_SETTINGS)
        self._cur = self._con.cursor()
        self.name = name

    def execute(self, query):
        print(self.name)
        self._cur.execute(query)
        print('query:', self._cur.query)
        print('stat_msg:', self._cur.statusmessage)
        print()


def start_serializable_transaction(db: Database):
    query = "BEGIN TRANSACTION ISOLATION LEVEL SERIALIZABLE;"
    db.execute(query)


def update_data_first(db: Database):
    query = "UPDATE serialize_error SET age=18 where name='vova';"
    db.execute(query)


def update_data_second(db: Database):
    query = "UPDATE serialize_error SET age=20 where name='vova';"
    db.execute(query)


def commit_transaction(db: Database):
    db.execute("COMMIT;")


def main():
    db1 = Database('1 database')
    db2 = Database('2 database')
    start_serializable_transaction(db1)
    start_serializable_transaction(db2)
    update_data_first(db1)
    update_data_second(db2)
    commit_transaction(db1)
    commit_transaction(db2)


if __name__ == "__main__":
    main()
